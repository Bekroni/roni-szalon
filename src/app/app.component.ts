import { Component, HostListener, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent{
  constructor(private swUpdate: SwUpdate){
    this.initializeApp();
  }

  public installPrompt = null;
  promptEvent: any;
  showButton = false;
  title = 'Letöltés applikációként';

@HostListener('window:beforeinstallprompt', ['$event'])
onbeforeinstallprompt(e) {
  e.preventDefault();
  this.promptEvent = e;
  this.showButton = true;
}

public addToHomeScreen() {
  this.showButton = false;
  this.promptEvent.prompt();
  this.promptEvent.userChoice
    .then((choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt');
      } else {
        console.log('User dismissed the A2HS prompt');
      }
      this.promptEvent = null;
    });
}

public installPWA() {
  this.promptEvent.prompt();
}

public shouldInstall(): boolean {
  return !this.isRunningStandalone() && this.promptEvent;
}

public isRunningStandalone(): boolean {
  return (window.matchMedia('(display-mode: standalone)').matches);
}

initializeApp(): void {
  if (this.swUpdate.available){
    this.swUpdate.available.subscribe(() => {
      if (confirm('Új verzió érhető el. Betölti?'))
        window.location.reload();
    });
  }
}
}