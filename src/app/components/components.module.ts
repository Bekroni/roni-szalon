import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalonComponent } from './salon/salon.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [
    SalonComponent,
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    SalonComponent
  ],
  entryComponents: []
})
export class ComponentsModule { }
