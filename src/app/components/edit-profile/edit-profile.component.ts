import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global/global.service';
import { ProfileService } from 'src/app/services/profile/profile.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {

  @Input() profile;
  isSubmitted = false;

  constructor(
    private profileService : ProfileService,
    private global: GlobalService
    ) { }

  ngOnInit() {}

  async onSubmit(form: NgForm){
    try {
      if(!form.valid){
        return;
      }
      this.isSubmitted = true;
      await this.profileService.updateProfile(this.profile, form.value);
      this.global.modalDismiss();
      this.isSubmitted = false;
    } catch(e) {
      console.log(e);
      this.global.errorToast();
    }
  }

}
