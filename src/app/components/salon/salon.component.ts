import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-salon',
  templateUrl: './salon.component.html',
  styleUrls: ['./salon.component.scss'],
})
export class SalonComponent implements OnInit {

  @Input() service: any;

  constructor() { }

  ngOnInit() {}

}
