import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(
    private authService: AuthService,
    private router: Router,
    private alertCtrl: AlertController) {

  }

  async canLoad(
    route: Route,
    segments: UrlSegment[]): Promise<boolean> {
      const roleType = route.data.type;
      try {
        const type = await this.authService.checkUserAuth();
        if(type) {
          if(type == roleType) return true;
          else {
            let url;
            if(type == 'user') this.router.navigateByUrl('/tabs');
            if(type == 'admin') this.router.navigateByUrl('/admin');
          }
        } else {
          this.checkForAlert(roleType);
        }
      } catch(e){
        console.log(e);
        this.checkForAlert(roleType);
      }
  }

  async checkForAlert(roleType){
    const id = await this.authService.getId();
    if(id) {
      console.log('alert: ', id);
      this.showAlert(roleType);
    } else {
      this.authService.logout();
      this.router.navigateByUrl('/login');
    }
  }

  showAlert(role){
    this.alertCtrl.create({
      header: 'Autentikációs hiba',
      message: 'Kérlek ellenőrizd az internet kapcsolatodat, és próbáld újra',
      buttons: [
        {
          text: 'Kijelentkezés',
          handler: () => {
            this.authService.logout();
            this.router.navigateByUrl('/login');
          }
        },
        {
          text: 'Újra',
          handler: () => {
            if(role == 'admin') this.router.navigateByUrl('/admin');
          }
        }
      ]
    }).then(alertEl => alertEl.present());
  }
}

