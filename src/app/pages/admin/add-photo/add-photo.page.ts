import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-photo',
  templateUrl: 'add-photo.page.html',
  styleUrls: ['add-photo.page.scss'],
})
export class AddPhotoPage implements OnInit {

  newImage: any;

  constructor() { }

  ngOnInit() {
  }

}
