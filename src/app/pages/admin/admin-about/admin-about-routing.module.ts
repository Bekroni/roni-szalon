import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminAboutPage } from './admin-about.page';

const routes: Routes = [
  {
    path: '',
    component: AdminAboutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminAboutPageRoutingModule {}
