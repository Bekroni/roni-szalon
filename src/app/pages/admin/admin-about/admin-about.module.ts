import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminAboutPageRoutingModule } from './admin-about-routing.module';

import { AdminAboutPage } from './admin-about.page';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminAboutPageRoutingModule,
    HttpClientModule
  ],
  declarations: [AdminAboutPage]
})
export class AdminAboutPageModule {}
