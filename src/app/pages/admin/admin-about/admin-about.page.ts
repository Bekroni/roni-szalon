import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { RealtimeDatabaseService, SalonData } from 'src/app/services/realtimeDatabase/realtime-database.service';

@Component({
  selector: 'app-admin-about',
  templateUrl: './admin-about.page.html',
  styleUrls: ['./admin-about.page.scss'],
})
export class AdminAboutPage implements OnInit {

  intro = '';
  aboutMe = '';
  datasFromServer: SalonData[];
  
  constructor(
    private realtimeDb: RealtimeDatabaseService,
    private globalServ: GlobalService
  ) {
    this.getIntroFromServer();
   }

  ngOnInit() {
  }

  async getIntroFromServer(){
    var getDatas: SalonData[];
    await this.realtimeDb.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.datasFromServer = getDatas;

    this.aboutMe = this.datasFromServer[0].intro;
  }

  saveIntro(){
    this.realtimeDb.sendDataToServer('intro', this.intro);
    let msg = 'A bemutatkozás sikeresen mentve lett';
    this.globalServ.showToast(msg, 'success', 'bottom', 4000);
    window.location.reload();
  }

}
