import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminAppointmentsPageRoutingModule } from './admin-appointments-routing.module';

import { AdminAppointmentsPage } from './admin-appointments.page';
import { NgCalendarModule } from 'ionic2-calendar';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCalendarModule,
    AdminAppointmentsPageRoutingModule,
    HttpClientModule
  ],
  declarations: [AdminAppointmentsPage]
})
export class AdminAppointmentsPageModule {}
