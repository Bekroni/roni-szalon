import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonDatetime, NavController } from '@ionic/angular';
import { format, parseISO } from 'date-fns';
import { CalendarComponent } from 'ionic2-calendar';
import { CalendarMode, Step } from 'ionic2-calendar/calendar';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GlobalService } from 'src/app/services/global/global.service';
import { Appointment, RealtimeDatabaseService, SalonData } from 'src/app/services/realtimeDatabase/realtime-database.service';
import { AllUsersService } from 'src/app/services/allUsers/all-users.service';
import { hu } from 'date-fns/locale';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-appointments',
  templateUrl: './admin-appointments.page.html',
  styleUrls: ['./admin-appointments.page.scss'],
})
export class AdminAppointmentsPage implements OnInit {

  isWeekday = (dateString: string) => {
    const date = new Date(dateString);
    const utcDay = date.getUTCDay();
    return utcDay !== 0 && utcDay !== 6;
  };

  @ViewChild(IonDatetime) dateTime : IonDatetime;
  @ViewChild(CalendarComponent) myCalendar : CalendarComponent;

  service = '';
  showPicker = false;
  showTimePicker = false;
  dateValue = format(new Date(), 'yyyy-MM-dd');
  formattedString = '';
  setTime = format(new Date(), 'HH:mm');
  setEndTime = new Date();
  formattedTime = '';
  allAppointments: Observable<Appointment[]>;
  userList: User[];
  selectedUserUid;
  openReserveAppointment: boolean = false;
  openDeleteAppointment: boolean = false;
  _yesterday;
  sorted = [];
  appsWithName = [];
  minDate = new Date().toISOString();
  newList: Appointment[];
  eventSource = [];
  gelLakkMinutesFromServer;
  mukoromMinutesFromServer;
  pedikurMinutesFromServer;
  datasFromServer: SalonData[];
  canBeSave: boolean;

  hourValue = ['8','9','10','11','12','13','14','15','16','17','18'];
  minuteValue = ['0','30'];

  calendar = {
    mode: 'week' as CalendarMode,
    currentDate: new Date(),
    step: 30 as Step,
    startHour: 8,
    endHour: 21,
    startingDayWeek: 1
  };

  public eventWithName = {
    key: '',
    service: '',
    date: '',
    startTime: '',
    endTime: '',
    uid: '',
    name: ''
  };

  public event = {
    key: '',
    service: '',
    date: '',
    startTime: '',
    endTime: '',
    uid: '',
  };

  public getEvent = {
    key: '',
    service: '',
    date: '',
    startTime: '',
    endTime: '',
    uid: '',
  };

  constructor(
    public navCtrl: NavController,
    public authService: AuthService,
    private realtimeDb: RealtimeDatabaseService,
    private globalServ: GlobalService,
    private alertCtrl: AlertController,
    private users: AllUsersService,
    private router: Router,
    @Inject(LOCALE_ID) private locale: string 
  ) {
    this.allAppointments = this.realtimeDb.getRealtimeDbAppointments();
    this.getAppointments();
    this.getUsers();
    this.setToday();
    this.getAllInformation();
    this.setAppointments();
   }

  ngOnInit() {
  }

  openReserveApp(){
    this.openReserveAppointment = !this.openReserveAppointment;
  }

  openDeleteApp(){
    this.openDeleteAppointment = !this.openDeleteAppointment;
  }

  serviceChanged(value){
    this.service = value;
  }

  customerChanged(value){
    this.selectedUserUid = value;
    console.log(this.selectedUserUid);
  }

  async getUsers(){
    var users: User[];
    await this.users.getUsers().then(value => {
      users = value as User[];
    });

    this.userList = users;

    this.userList?.forEach((element, index) => {
      if (element.type == 'admin') this.userList.splice(index, 1);
    })
  }

  async getAllInformation(){
    var getDatas: SalonData[];
    await this.realtimeDb.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.datasFromServer = getDatas;

    this.gelLakkMinutesFromServer = this.datasFromServer[0].gelLakkTime;
    this.mukoromMinutesFromServer = this.datasFromServer[0].mukoromTime;
    this.pedikurMinutesFromServer = this.datasFromServer[0].pedikurTime;
  }

  async getAppointments(){
    var getAppointm: Appointment[];
    await this.realtimeDb.getApps().then(value => {
      getAppointm = value as Appointment[];
    });

    this.newList = getAppointm;

    for (const l of this.newList){
      this.getEvent.service = l.service;
      this.getEvent.date = l.date;
      this.getEvent.startTime = l.time;
      this.getEvent.endTime = l.end;
      this.getEvent.uid = l.uid;
      this.getEvent.key = l.key;

      let eventCopy = {
        service: this.getEvent.service,
        startTime: new Date(this.getEvent.startTime),
        endTime: new Date(this.getEvent.endTime),
        uid: this.getEvent.uid
      }

      this.eventSource.push(eventCopy);
    }

    this.myCalendar.loadEvents();
  }

  setToday(){
    this.formattedString = '';
    this.formattedTime = '';
  }

  dateChanged(value){
    this.calendar.currentDate = value;
    this.event.date = value;
    this.formattedString = format(parseISO(value), 'yyyy. MMMMMMMMMMMMMM d.', { locale: hu });
  }

  timeChanged(value){
    this.setTime = value;
    this.event.startTime = value;
    this.formattedTime = format(parseISO(value), 'HH:mm', { locale: hu });
  }

  getEndTime(){
    if (this.event.service == 'Gél lakk'){
      var minutes = this.gelLakkMinutesFromServer;
      var start = new Date(this.event.startTime);
      var end = new Date(start.getTime() + minutes*60000);
    } else if (this.event.service == 'Műköröm'){
      var minutes = this.mukoromMinutesFromServer;
      var start = new Date(this.event.startTime);
      var end = new Date(start.getTime() + minutes*60000);
    } else if (this.event.service == 'Pedikűr'){
      var minutes = this.pedikurMinutesFromServer;
      var start = new Date(this.event.startTime);
      var end = new Date(start.getTime() + minutes*60000);
    }
    return end;
  }

  endTime(){
    this.setEndTime = this.getEndTime();
    this.event.endTime = this.setEndTime.toString();
  }

  addAppointment() {
    this.canBeSave = false;
    let eventCopy = {
      service: this.event.service,
      date: new Date(this.event.date),
      startTime: new Date(this.event.startTime),
      endTime: new Date(this.getEndTime())
    }

    for (var event of this.eventSource){
      if (((eventCopy.startTime >= event.startTime) && (eventCopy.startTime < event.endTime)) || ((eventCopy.endTime <= event.endTime) && (eventCopy.endTime > event.startTime))){
        let msg = '';
        msg = 'Erre az időpontra már nem lehetséges a foglalás';
        this.globalServ.showToast(msg, 'danger', 'bottom', 4000);
        this.canBeSave = false;
        break;
      } else {
        this.canBeSave = true;
      }
    }

    if (this.event.service == '' || this.formattedString == '' || this.formattedTime == '' || this.selectedUserUid == null) {
      let msg = '';
      msg = 'Tölts ki minden adatot a mentéshez';
      this.globalServ.missingToast(msg);
      this.canBeSave = false;
    }


    if (this.canBeSave == true){
      let msg = 'Az időpont sikeresen mentésre került';
      this.globalServ.showToast(msg, 'success', 'bottom', 4000);
      this.eventSource.push(eventCopy);
      this.myCalendar.loadEvents();
      this.sendAppointment(eventCopy);
    }
  }

  sendAppointment(newEvent){
    let setEvent = {
      service: newEvent.service,
      date: newEvent.date.toString(),
      startTime: newEvent.startTime.toString(),
      endTime: newEvent.endTime.toString()
    }
    this.realtimeDb.sendAppointment(setEvent, this.selectedUserUid);
    window.location.reload();
  }

  next(){
    this.myCalendar.slideNext();
  }

  back(){
    this.myCalendar.slidePrev();
  }

  onTimeSelected(ev){
    let selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toString());
  }

  async onEventSelected(event) {
    var year = event.startTime.getUTCFullYear();
    var month = ('0' + (event.startTime.getUTCMonth() + 1)).slice(-2);
    var day = ('0' + event.startTime.getUTCDate()).slice(-2);
    var date = [year,month,day].join('-');
    var hoursAndMinutes = (('0' + event.startTime.getHours()).slice(-2)) + ':' + (('0' + event.startTime.getMinutes()).slice(-2));
    let service = event.service;
    let uid = event.uid;
    let name;

    this.userList?.forEach((element, index) => {
      if (element.uid == uid) name = element.name;
    })

    const alert = await this.alertCtrl.create({
        message: 'Vendég neve: ' + name + '<br><br>Szolgáltatás: ' + service + '<br><br>Időpont: ' + date + ' ' + hoursAndMinutes,
        buttons: ['OK'],
      });
      alert.present();
  }

  async setAppointments(){
    this._yesterday = new Date();
    this._yesterday.setDate(this._yesterday.getDate() - 1);
    var year = this._yesterday.getUTCFullYear();
    var month = ('0' + (this._yesterday.getUTCMonth() + 1)).slice(-2);
    var day = ('0' + this._yesterday.getUTCDate()).slice(-2);
    var yesterdayDate = [year,month,day].join('-');

    var getAppointm: Appointment[];
    await this.realtimeDb.getApps().then(value => {
      getAppointm = value as Appointment[];
    });

    for (const l of getAppointm){
      this.getEvent.service = l.service;
      this.getEvent.date = l.date;
      this.getEvent.startTime = l.time;
      this.getEvent.endTime = l.end;
      this.getEvent.uid = l.uid;
      this.getEvent.key = l.key;

      let eventCopy: Appointment = {
        service: this.getEvent.service,
        date: this.getEvent.date,
        time: new Date(this.getEvent.startTime),
        end: new Date(this.getEvent.endTime),
        uid: this.getEvent.uid,
        key: this.getEvent.key
      }

      var name;

      this.userList?.forEach((element, index) => {
        if (element.uid == eventCopy.uid) name = element.name;
      })

      let ev = {
        service: this.getEvent.service,
        date: this.getEvent.date,
        time: new Date(this.getEvent.startTime),
        end: new Date(this.getEvent.endTime),
        uid: this.getEvent.uid,
        key: this.getEvent.key,
        name: name
      }

      var appYear = eventCopy.time.getUTCFullYear();
      var appMonth = ('0' + (eventCopy.time.getUTCMonth() + 1)).slice(-2);
      var appDay = ('0' + eventCopy.time.getUTCDate()).slice(-2);
      var appDate = [appYear,appMonth,appDay].join('-');

      if (appDate > yesterdayDate){
        this.appsWithName.push(ev);
      }
    }
    this.sorted = this.appsWithName.sort((a, b) => a.time.getTime() - b.time.getTime());
  }

  remove(id){
    const getId = id;
    this.realtimeDb.deleteAppointment(getId);
    window.location.reload();
  }

}
