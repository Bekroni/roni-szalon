import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminContactPage } from './admin-contact.page';

const routes: Routes = [
  {
    path: '',
    component: AdminContactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminContactPageRoutingModule {}
