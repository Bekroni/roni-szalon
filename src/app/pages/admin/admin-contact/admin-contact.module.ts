import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminContactPageRoutingModule } from './admin-contact-routing.module';

import { AdminContactPage } from './admin-contact.page';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminContactPageRoutingModule,
    HttpClientModule
  ],
  declarations: [AdminContactPage]
})
export class AdminContactPageModule {}
