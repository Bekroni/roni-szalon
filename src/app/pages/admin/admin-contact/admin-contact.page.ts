import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { RealtimeDatabaseService, SalonData } from 'src/app/services/realtimeDatabase/realtime-database.service';

@Component({
  selector: 'app-admin-contact',
  templateUrl: './admin-contact.page.html',
  styleUrls: ['./admin-contact.page.scss'],
})
export class AdminContactPage implements OnInit {

  datasFromServer: SalonData[];
  getContact = '';
  getEmail;
  getTel;
  contact;
  email;
  tel;

  constructor(
    private realtimeDb: RealtimeDatabaseService,
    private globalServ: GlobalService
  ) {
    this.getAllInformation();
   }

  ngOnInit() {
  }

  async getAllInformation(){
    var getDatas: SalonData[];
    await this.realtimeDb.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.datasFromServer = getDatas;

    this.getContact = this.datasFromServer[0].contactIntro;
    this.getEmail = this.datasFromServer[0].email;
    this.getTel = this.datasFromServer[0].tel;
  }

  saveContactIntro(){
    this.realtimeDb.sendDataToServer('contactIntro', this.contact);
    let msg = 'A kapcsolat oldalon látható szöveg sikeresen mentve lett';
    this.globalServ.showToast(msg, 'success', 'bottom', 4000);
    window.location.reload();
  }

  saveEmail(){
    this.realtimeDb.sendDataToServer('email', this.email);
    let msg = 'A kapcsolaton oldalon látható e-mail cím sikeresen mentve lett';
    this.globalServ.showToast(msg, 'success', 'bottom', 4000);
    window.location.reload();
  }
  
  saveTel(){
    this.realtimeDb.sendDataToServer('tel', this.tel);
    let msg = 'A kapcsolat oldalon látható telefonszám sikeresen mentve lett';
    this.globalServ.showToast(msg, 'success', 'bottom', 4000);
    window.location.reload();
  }

}
