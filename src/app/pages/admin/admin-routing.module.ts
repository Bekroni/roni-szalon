import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DayPilotModule } from '@daypilot/daypilot-lite-angular';
import { AdminPage } from './admin.page';


const routes: Routes = [
  {
    path: '',
    component: AdminPage
  },
  {
    path: 'add-photo',
    loadChildren: () => import('./add-photo/add-photo.module').then( m => m.AddPhotoPageModule)
  },
  {
    path: 'admin-appointments',
    loadChildren: () => import('./admin-appointments/admin-appointments.module').then( m => m.AdminAppointmentsPageModule)
  },
  {
    path: 'admin-about',
    loadChildren: () => import('./admin-about/admin-about.module').then( m => m.AdminAboutPageModule)
  },
  {
    path: 'admin-contact',
    loadChildren: () => import('./admin-contact/admin-contact.module').then( m => m.AdminContactPageModule)
  },
  {
    path: 'admin-services',
    loadChildren: () => import('./admin-services/admin-services.module').then( m => m.AdminServicesPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    DayPilotModule,
    HttpClientModule
  ],
  exports: [RouterModule],
})
export class AdminPageRoutingModule {}
