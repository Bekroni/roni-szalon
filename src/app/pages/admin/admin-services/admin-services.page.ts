import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { RealtimeDatabaseService, SalonData } from 'src/app/services/realtimeDatabase/realtime-database.service';

@Component({
  selector: 'app-admin-services',
  templateUrl: './admin-services.page.html',
  styleUrls: ['./admin-services.page.scss'],
})
export class AdminServicesPage implements OnInit {

  gelLakkMinutes;
  mukoromMinutes;
  pedikurMinutes;
  gelLakkMinutesFromServer;
  mukoromMinutesFromServer;
  pedikurMinutesFromServer;  
  datasFromServer: SalonData[];

  constructor(
    private realtimeDb: RealtimeDatabaseService,
    private globalServ: GlobalService
  ) {
    this.getAllInformation();
   }

  ngOnInit() {
  }

  async getAllInformation(){
    var getDatas: SalonData[];
    await this.realtimeDb.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.datasFromServer = getDatas;

    this.gelLakkMinutesFromServer = this.datasFromServer[0].gelLakkTime;
    this.mukoromMinutesFromServer = this.datasFromServer[0].mukoromTime;
    this.pedikurMinutesFromServer = this.datasFromServer[0].pedikurTime;
  }

  saveGelLakk(){
    this.realtimeDb.sendDataToServer('gelLakkTime', this.gelLakkMinutes);
    let msg = 'A gél lakk időtartama sikeresen mentve lett';
    this.globalServ.showToast(msg, 'success', 'bottom', 4000);
    window.location.reload();
  }

  saveMukorom(){
    this.realtimeDb.sendDataToServer('mukoromTime', this.mukoromMinutes);
    let msg = 'A műköröm időtartama sikeresen mentve lett';
    this.globalServ.showToast(msg, 'success', 'bottom', 4000);
    window.location.reload();
  }

  savePedikur(){
    this.realtimeDb.sendDataToServer('pedikurTime', this.pedikurMinutes);
    let msg = 'A pedikűr időtartama sikeresen mentve lett';
    this.globalServ.showToast(msg, 'success', 'bottom', 4000);
    window.location.reload();
  }

  gelLakkChanged(value){
    this.gelLakkMinutes = value;
  }

  mukoromChanged(value){
    this.mukoromMinutes = value;
  }

  pedikurChanged(value){
    this.pedikurMinutes = value;
  }

}
