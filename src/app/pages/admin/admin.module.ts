import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgCalendarModule } from 'ionic2-calendar';
import { SwiperModule } from 'swiper/angular';

import { IonicModule } from '@ionic/angular';

import { AdminPageRoutingModule } from './admin-routing.module';

import { AdminPage } from './admin.page';
import { AppointmentPageRoutingModule } from '../tabs/appointment/appointment-routing.module';
import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminPageRoutingModule,
    NgCalendarModule,
    SwiperModule,
    AppointmentPageRoutingModule,
    ScheduleModule,
    HttpClientModule
  ],
  declarations: [AdminPage]
})
export class AdminPageModule {}
