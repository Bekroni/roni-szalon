import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GlobalService } from 'src/app/services/global/global.service';


@Component({
  selector: 'app-admin',
  templateUrl: 'admin.page.html',
  styleUrls: ['admin.page.scss'],
})

export class AdminPage implements OnInit, OnDestroy {

  constructor(
    public navCtrl: NavController,
    public authService: AuthService,
    private globalServ: GlobalService
  ) {
     }

  ngOnInit() {
    this.globalServ.customStatusbar(true);
  }

  logout(){
    this.globalServ.showLoader();
    this.authService.logout().then(() => {
      this.navCtrl.navigateRoot('/login');
      this.globalServ.hideLoader();
    })
    .catch(e => {
      console.log(e);
      this.globalServ.hideLoader();
      this.globalServ.errorToast('Kijelentkezés sikertelen!');
    });
  }

  ngOnDestroy() {
    this.globalServ.customStatusbar();
  }
}