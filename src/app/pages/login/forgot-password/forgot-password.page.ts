import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GlobalService } from 'src/app/services/global/global.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: 'forgot-password.page.html',
  styleUrls: ['forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  isLoading = false;

  constructor(private navCtrl: NavController, private auth: AuthService, private global: GlobalService) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm){
    console.log(form.value);
    if(!form.valid) return;
    this.isLoading = true;
    setTimeout(() => {
      this.auth.resetPassword(form.value.email).then(() => {
        this.global.successToast('Az új jelszóhoz szükséges link el lett küldve a megadott email címre');
        this.isLoading = false;
        this.navCtrl.back();
      })
      .catch(e => {
        console.log(e);
        this.global.showAlert('Valamilyen hiba történt');
      });
    }, 3000);
  }

}
