import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GlobalService } from 'src/app/services/global/global.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {

  type = 'password';
  iconName = 'eye-outline';
  isLogin = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private global: GlobalService
    ) { }

  ngOnInit() {
    this.isLoggedIn();
  }

  async isLoggedIn(){
    try{
      this.global.showLoader();
      const val = this.authService.getId();
      if(val) this.navigate();
      this.global.hideLoader();
    } catch(e) {
      console.log(e);
      this.global.hideLoader();
    }
  }

  changeType() {
    if(this.iconName == 'eye-outline') {
      this.type = 'text';
      this.iconName = 'eye-off-outline';
    } else {
      this.type = 'password';
      this.iconName = 'eye-outline';
    }
  }

  onSubmit(form: NgForm) {
    console.log(form);
    if(!form.valid) return;
    this.login(form);
  }

  login(form){
    this.isLogin = true;
    this.authService.login(form.value.email, form.value.password)
    .then(data => {
      console.log(data);
      this.navigate();
      this.isLogin = false;
      form.reset();
    })
    .catch(e => {
      console.log(e);
      this.isLogin = false;
      let msg: string = 'Nem sikerült a bejelentkezés';
      if(e.code == 'auth/user-not-found') msg = 'Nincs ilyen e-mail cím a rendszerben';
      else if(e.code == 'auth/wrong-password') msg= 'Helytelen jelszó';
      this.global.showAlert(msg);
    })
  }

  navigate(){
    this.router.navigateByUrl('/tabs');
  }
}
