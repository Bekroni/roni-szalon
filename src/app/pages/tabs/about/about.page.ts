import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { RealtimeDatabaseService, SalonData } from 'src/app/services/realtimeDatabase/realtime-database.service';

@Component({
  selector: 'app-about',
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss'],
})

export class AboutPage implements OnInit {

  constructor(
    private realtimeDb: RealtimeDatabaseService
  ) {
    this.getIntro();
  }

  aboutMe;
  datasFromServer: SalonData[];

  ngOnInit(): void {
    
  }

  async getIntro(){
    var getDatas: SalonData[];
    await this.realtimeDb.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.datasFromServer = getDatas;

    this.aboutMe = this.datasFromServer[0].intro;
  }
}
