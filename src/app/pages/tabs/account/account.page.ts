import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { EditProfileComponent } from 'src/app/components/edit-profile/edit-profile.component';
import { GlobalService } from 'src/app/services/global/global.service';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { Appointment, RealtimeDatabaseService } from 'src/app/services/realtimeDatabase/realtime-database.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { format, parseISO } from 'date-fns';
import { hu } from 'date-fns/locale';

@Component({
  selector: 'app-account',
  templateUrl: 'account.page.html',
  styleUrls: ['account.page.scss'],
})

export class AccountPage implements OnInit {

  profile: any = {};
  isLoading: boolean;
  profileSub: Subscription;
  isLogin: false;
  allAppointments: Observable<Appointment[]>;
  _uid;
  _yesterday;
  appsList: Appointment[] = [];
  sorted: Appointment[] = [];

  public getEvent = {
    date: '',
    service: '',
    startTime: '',
    endTime: '',
    uid: '',
    key: ''
  };

  constructor(
    private global: GlobalService,
    private profileService: ProfileService,
    private storageService: StorageService,
    private realtimeDb: RealtimeDatabaseService
  ) { 
    this.allAppointments = realtimeDb.getRealtimeDbAppointments();
    this.checkUserId();
    this.setAppointments();
  }

  async checkUserId(){
    this._uid = (await this.storageService.getStorage('uid')).value;
  }

  ngOnInit() {
    this.profileSub = this.profileService.profile.subscribe(profile => {
      this.profile = profile;
    });
    this.getData();
  }

  async setAppointments(){
    this._yesterday = new Date();
    this._yesterday.setDate(this._yesterday.getDate() - 1);
    var year = this._yesterday.getUTCFullYear();
    var month = ('0' + (this._yesterday.getUTCMonth() + 1)).slice(-2);
    var day = ('0' + this._yesterday.getUTCDate()).slice(-2);
    var yesterdayDate = [year,month,day].join('-');

    var getAppointm: Appointment[];
    await this.realtimeDb.getApps().then(value => {
      getAppointm = value as Appointment[];
    });

    for (const l of getAppointm){
      this.getEvent.service = l.service;
      this.getEvent.date = l.date;
      this.getEvent.startTime = l.time;
      this.getEvent.endTime = l.end;
      this.getEvent.uid = l.uid;
      this.getEvent.key = l.key;

      let eventCopy: Appointment = {
        service: this.getEvent.service,
        date: this.getEvent.date,
        time: new Date(this.getEvent.startTime),
        end: new Date(this.getEvent.endTime),
        uid: this.getEvent.uid,
        key: this.getEvent.key
      }

      var appYear = eventCopy.time.getUTCFullYear();
      var appMonth = ('0' + (eventCopy.time.getUTCMonth() + 1)).slice(-2);
      var appDay = ('0' + eventCopy.time.getUTCDate()).slice(-2);
      var appDate = [appYear,appMonth,appDay].join('-');

      if (appDate > yesterdayDate){
        this.appsList.push(eventCopy);
      }
    }
    this.sorted = this.appsList.sort((a, b) => a.time.getTime() - b.time.getTime());
  }

  async getData(){
    try{
      this.isLoading = true;
      await this.profileService.getProfile();
      this.isLoading = false;
    } catch(e){
      this.isLoading = false;
      console.log(e);
      this.global.errorToast();
    }
  }

  async editProfile(){
    const options = {
      component: EditProfileComponent,
      componentProps: {
        profile: this.profile
      },
      cssClass: 'custom-modal',
      swipeToClose: true
    };
    const modal = await this.global.createModal(options);
  }

  ngOnDestroy() {
    if(this.profileSub) this.profileSub.unsubscribe();
  }

  remove(id){
    const getId = id;
    this.realtimeDb.deleteAppointment(getId);
    window.location.reload();
  }
}
