import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DayPilotModule } from '@daypilot/daypilot-lite-angular';

import { AppointmentPage } from './appointment.page';

const routes: Routes = [
  {
    path: '',
    component: AppointmentPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    DayPilotModule,
    HttpClientModule
  ],
  exports: [RouterModule],
})
export class AppointmentPageRoutingModule {}
