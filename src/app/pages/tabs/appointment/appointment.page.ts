import { Component, OnInit, ViewChild, Inject, LOCALE_ID } from '@angular/core';
import { AlertController, IonDatetime } from '@ionic/angular';
import { format, parseISO } from 'date-fns';
import { hu } from 'date-fns/locale';
import { Observable } from 'rxjs';
import { Appointment, RealtimeDatabaseService, SalonData } from 'src/app/services/realtimeDatabase/realtime-database.service';
import { CalendarComponent } from 'ionic2-calendar';
import { GlobalService } from 'src/app/services/global/global.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { CalendarMode } from 'ionic2-calendar/calendar';
import { Step } from 'ionic2-calendar/calendar';


@Component({
  selector: 'app-appointment',
  templateUrl: 'appointment.page.html',
  styleUrls: ['appointment.page.scss'],
})

export class AppointmentPage implements OnInit {

  isWeekday = (dateString: string) => {
    const date = new Date(dateString);
    const utcDay = date.getUTCDay();
    return utcDay !== 0 && utcDay !== 6;
  };

  @ViewChild(IonDatetime) dateTime : IonDatetime;
  @ViewChild(CalendarComponent) myCalendar : CalendarComponent;

  service = '';
  showPicker = false;
  showTimePicker = false;
  dateValue = format(new Date(), 'yyyy-MM-dd');
  formattedString = '';
  setTime = format(new Date(), 'HH:mm');
  setEndTime = new Date();
  formattedTime = '';
  allAppointments: Observable<Appointment[]>;
  _uid;
  gelLakkMinutes;
  mukoromMinutes;
  pedikurMinutes;
  canBeSave: boolean;
  datasFromServer: SalonData[];

  public event = {
    key: '',
    service: '',
    date: '',
    startTime: '',
    endTime: '',
    uid: '',
  };

  public getEvent = {
    key: '',
    service: '',
    date: '',
    startTime: '',
    endTime: '',
    uid: '',
  };

  hourValue = ['8','9','10','11','12','13','14','15','16','17','18'];
  minuteValue = ['0','30'];

  newList: Appointment[];

  minDate = new Date().toISOString();

  eventSource = [];

  calendar = {
    mode: 'week' as CalendarMode,
    currentDate: new Date(),
    step: 30 as Step,
    startHour: 8,
    endHour: 21,
    startingDayWeek: 1
  };

  constructor(
    private realtimeDb: RealtimeDatabaseService,
    private globalServ: GlobalService,
    private alertCtrl: AlertController,
    private storageService: StorageService,
    @Inject(LOCALE_ID) private locale: string 
    ) {
      this.checkUserId();
      this.allAppointments = this.realtimeDb.getRealtimeDbAppointments();
      this.getAppointments();
      this.getAllInformation();
    }

  ngOnInit() {
  }

  async checkUserId(){
    this._uid = (await this.storageService.getStorage('uid')).value;
  }

  async getAppointments(){
    var getAppointm: Appointment[];
    await this.realtimeDb.getApps().then(value => {
      getAppointm = value as Appointment[];
    });

    this.newList = getAppointm;

    for (const l of this.newList){
      this.getEvent.service = l.service;
      this.getEvent.date = l.date;
      this.getEvent.startTime = l.time;
      this.getEvent.endTime = l.end;
      this.getEvent.uid = l.uid;
      this.getEvent.key = l.key;

      let eventCopy = {
        service: this.getEvent.service,
        startTime: new Date(this.getEvent.startTime),
        endTime: new Date(this.getEvent.endTime),
        uid: this.getEvent.uid
      }

      this.eventSource.push(eventCopy);
    }

    this.myCalendar.loadEvents();
  }

  async getAllInformation(){
    var getDatas: SalonData[];
    await this.realtimeDb.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.datasFromServer = getDatas;

    this.gelLakkMinutes = this.datasFromServer[0].gelLakkTime;
    this.mukoromMinutes = this.datasFromServer[0].mukoromTime;
    this.pedikurMinutes = this.datasFromServer[0].pedikurTime;
  }

  serviceChanged(value){
    this.service = value;
  }

  dateChanged(value){
    this.calendar.currentDate = value;
    this.event.date = value;
    this.formattedString = format(parseISO(value), 'yyyy. MMMMMMMMMMMMMM d.', { locale: hu });
  }

  timeChanged(value){
    this.setTime = value;
    this.event.startTime = value;
    this.formattedTime = format(parseISO(value), 'HH:mm', { locale: hu });
  }

  getEndTime(){
    if (this.event.service == 'Gél lakk'){
      var minutes = this.gelLakkMinutes;
      var start = new Date(this.event.startTime);
      var end = new Date(start.getTime() + minutes*60000);
    } else if (this.event.service == 'Műköröm'){
      var minutes = this.mukoromMinutes;
      var start = new Date(this.event.startTime);
      var end = new Date(start.getTime() + minutes*60000);
    } else if (this.event.service == 'Pedikűr'){
      var minutes = this.pedikurMinutes;
      var start = new Date(this.event.startTime);
      var end = new Date(start.getTime() + minutes*60000);
    }
    return end;
  }

  endTime(){
    this.setEndTime = this.getEndTime();
    this.event.endTime = this.setEndTime.toString();
  }

  addAppointment() {
    this.canBeSave = false;
    let eventCopy = {
      service: this.event.service,
      date: new Date(this.event.date),
      startTime: new Date(this.event.startTime),
      endTime: new Date(this.getEndTime())
    }

    for (var event of this.eventSource){
      if (((eventCopy.startTime >= event.startTime) && (eventCopy.startTime < event.endTime)) || ((eventCopy.endTime <= event.endTime) && (eventCopy.endTime > event.startTime))){
        let msg = '';
        msg = 'Erre az időpontra már nem lehetséges a foglalás';
        this.globalServ.showToast(msg, 'danger', 'bottom', 4000);
        this.canBeSave = false;
        break;
      } else {
        this.canBeSave = true;
      }
    }

    if (this.event.service == '' || this.formattedString == '' || this.formattedTime == '') {
      let msg = '';
      msg = 'Tölts ki minden adatot a mentéshez';
      this.globalServ.missingToast(msg);
      this.canBeSave = false;
    }


    if (this.canBeSave == true){
      let msg = 'Az időpont sikeresen mentésre került';
      this.globalServ.showToast(msg, 'success', 'bottom', 4000);
      this.eventSource.push(eventCopy);
      this.myCalendar.loadEvents();
      this.sendAppointment(eventCopy);
    }
  }

  sendAppointment(newEvent){
    let setEvent = {
      service: newEvent.service,
      date: newEvent.date.toString(),
      startTime: newEvent.startTime.toString(),
      endTime: newEvent.endTime.toString()
    }
    this.realtimeDb.sendAppointment(setEvent, this._uid);
    window.location.reload();
  }

  next(){
    this.myCalendar.slideNext();
  }

  back(){
    this.myCalendar.slidePrev();
  }

  onTimeSelected(ev){
    let selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toString());
  }

  async onEventSelected(event) {
    var year = event.startTime.getUTCFullYear();
    var month = ('0' + (event.startTime.getUTCMonth() + 1)).slice(-2);
    var day = ('0' + event.startTime.getUTCDate()).slice(-2);
    var date = [year,month,day].join('-');
    let service = event.service;

    if (this._uid == event.uid){
      const alert = await this.alertCtrl.create({
        message: 'Szolgáltatás: ' + service + '<br><br>Foglalt időpont: ' + date,
        buttons: ['OK'],
      });
      alert.present();
    } else {
      const alert = await this.alertCtrl.create({
        message: 'Más felhasználó által foglalt időpont',
        buttons: ['OK'],
      });
      alert.present();      
    }
  }

}
