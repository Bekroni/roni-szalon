import { Component, OnInit } from '@angular/core';
import { RealtimeDatabaseService, SalonData } from 'src/app/services/realtimeDatabase/realtime-database.service';


@Component({
  selector: 'app-contact',
  templateUrl: 'contact.page.html',
  styleUrls: ['contact.page.scss'],
})

export class ContactPage implements OnInit {

  constructor(
    private realtimeDb: RealtimeDatabaseService
  ) {}

  datasFromServer: SalonData[];
  contact;
  email = '';
  tel = '';

  ngOnInit(): void {
    this.getIntro();
  }

  async getIntro(){
    var getDatas: SalonData[];
    await this.realtimeDb.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.datasFromServer = getDatas;

    this.contact = this.datasFromServer[0].contactIntro;
    this.email = this.datasFromServer[0].email;
    this.tel = this.datasFromServer[0].tel;
  }
}
