import { Component, HostListener, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GlobalService } from 'src/app/services/global/global.service';
import { ProfileService } from 'src/app/services/profile/profile.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  encapsulation: ViewEncapsulation.None
})


export class HomePage implements OnInit, OnDestroy {

  banners: any[] = [];
  services: any[] = [];
  profile: any = {};
  profileSub: Subscription;
  isLoading: boolean;
  deferredPrompt: any;
  showButton = false;

  constructor(
    private api: ApiService,
    private global: GlobalService,
    private authService: AuthService,
    private navCtrl: NavController,
    private profileService: ProfileService
  ) { }

  ngOnInit() {
    this.banners = this.api.banners;
    this.services = this.api.services;
    this.profileSub = this.profileService.profile.subscribe(profile => {
      this.profile = profile;
    });
    this.getData();
  }

  async getData() {
    try {
      this.isLoading = true;
      await this.profileService.getProfile();
      this.isLoading = false; 
    } catch(e) {
      this.isLoading = false;
      console.log(e);
      this.global.errorToast();
    }
  }

  confirmLogout(){
    this.global.showAlert(
      'Biztosan ki szeretnél jelentkezni?',
      'Erősítsd meg',
      [{
        text: 'Nem',
        role: 'cancel'
      }, {
        text: 'Igen',
        handler: () => {
          this.logout();
        }
      }]
    );
  }

  logout(){
    this.global.showLoader();
    this.authService.logout().then(() => {
      this.navCtrl.navigateRoot('/login');
      this.global.hideLoader();
    })
    .catch(e => {
      console.log(e);
      this.global.hideLoader();
      this.global.errorToast('Kijelentkezés sikertelen!');
    });
  }

  ngOnDestroy() {
      if(this.profileSub) this.profileSub.unsubscribe();
  }

}
