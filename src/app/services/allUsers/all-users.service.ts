import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AllUsersService {

  public userList: User[] = [];

  constructor(
    private db: AngularFirestore
  ) {
    this.toArray();
  }

  async toArray(){
    var users: User[];
    await this.getUsers().then(value => {
      users = value as User[];
    });

    this.userList = users;
  }

  getUsers(){
    return new Promise((resolve, reject) => {
      this.db.collection('users').valueChanges().subscribe(value => {
        resolve(value);
      })
    })
  }
}
