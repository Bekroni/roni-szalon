import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private adb: AngularFirestore
  ) {}

  collection(path, queryFn?){
    return this.adb.collection(path, queryFn);
  }

  banners = [
    {banner: 'assets/imgs/1.jpg', alt:"kék, cicás gél lakk"},
    {banner: 'assets/imgs/2.jpg', alt:"fehér és rózsaszín, francia műköröm"},
    {banner: 'assets/imgs/3.jpg', alt:"francia pedikűr"}
  ];

  services = [
    {
      cover: 'assets/imgs/1.jpg',
      name: 'Gél lakk',
      price: '4 800 Ft',
      short_name: 'gellakk'
    },
    {
      cover: 'assets/imgs/2.jpg',
      name: 'Műköröm',
      price: '5 800 Ft',
      short_name: 'mukorom'
    },
    {
      cover: 'assets/imgs/3.jpg',
      name: 'Pedikűr',
      price: '6 000 Ft',
      short_name: 'pedikur'
    }
  ]
}
