import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage'


@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor() { }

  setEventKey(key, value){
    Storage.set({key: key, value: value});
  }

  getEventKey(key){
    return Storage.get({key: key});
  }

  removeEventKey(key){
    Storage.remove({key: key});
  }
}
