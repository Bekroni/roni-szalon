import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { isPlatform } from '@ionic/angular';
import { StatusBar, Style } from '@capacitor/status-bar';
import { Capacitor } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  isLoading: boolean = false;

  constructor(
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController
  ) { }

  setLoader() {
    this.isLoading = !this.isLoading;
  }

  showAlert(message: string, header?, buttonArray?, inputs?){
    this.alertCtrl.create({
      header: header ? header : 'Autentikációs hiba',
      message: message,
      inputs: inputs ? inputs : [],
      buttons: buttonArray? buttonArray : ['Oké']
    })
    .then(alertEl => alertEl.present());
  }

  async showToast(msg, color, position, duration = 3000) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: duration,
      color: color,
      position: position
    });
    toast.present();
  }

  errorToast(msg?, duration = 4000){
    this.showToast(msg ? msg : 'Nincs internet elérés', 'danger', 'bottom', duration)
  }

  successToast(msg){
    this.showToast(msg, 'success', 'bottom');
  }

  missingToast(msg, duration = 4000){
    this.showToast(msg, 'warning', 'bottom', duration);
  }

  showLoader(msg?, spinner?){
//    this.isLoading = true;
    if(!this.isLoading) this.setLoader();
    return this.loadingCtrl.create({
      message: msg,
      spinner: spinner ? spinner : 'bubbles'
    }).then(res => {
      res.present().then(() => {
        if(!this.isLoading){
          res.dismiss().then(() => {
            console.log('abort presenting');
          });
        }
      })
    })
      .catch(e => {
        console.log('show loading error', e);
      });
    }

    hideLoader(){
//      this.isLoading = false;
      if(this.isLoading) this.setLoader();
      return this.loadingCtrl.dismiss()
      .then(() => console.log('dismissed'))
      .catch(e => console.log('error hide loader: ', e));
    }

    async createModal(options) {
      const modal = await this.modalCtrl.create(options);
      await modal.present();
      const {data} = await modal.onWillDismiss();
      if(data) return data;
    }

    modalDismiss(val?){
      let data: any = val ? val : null;
      this.modalCtrl.dismiss(data);
    }

    async customStatusbar(primaryColor?: boolean){
      if(Capacitor.getPlatform() != 'web'){
        await StatusBar.setStyle({style: primaryColor ? Style.Dark : Style.Light});
        if(isPlatform('android')) StatusBar.setBackgroundColor({color : primaryColor ? '#de0f17' : '#fffff'});
      }
    }
}
