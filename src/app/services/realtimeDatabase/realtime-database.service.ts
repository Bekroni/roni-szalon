import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';

export interface Appointment {
  service: string;
  date: any;
  time: any;
  end: any;
  uid: any;
  key: any;
}

export interface SalonData {
  intro: string;
  gelLakkTime: any;
  pedikurTime: any;
  mukoromTime: any;
  key: any;
  contactIntro: string;
  email: any;
  tel: any;
}

@Injectable({
  providedIn: 'root'
})
export class RealtimeDatabaseService {
  apps: Observable<Appointment[]>;
  datas: Observable<SalonData[]>

  public appList: Appointment[] = [];
  public dataList: SalonData[] = [];

  constructor(
    private realtimeDb: AngularFireDatabase,
    public authService: AuthService) {
      this.toArray();
    }

  async toArray(){
    var getAppointm: Appointment[];
    await this.getApps().then(value => {
      getAppointm = value as Appointment[];
    });

    this.appList = getAppointm;

    var getDatas: SalonData[];
    await this.getDatas().then(value => {
      getDatas = value as SalonData[];
    });

    this.dataList = getDatas;
  }

  getApps(){
    return new Promise((resolve, reject) => {
      this.realtimeDb.list('appointments').valueChanges().subscribe(value => {
        resolve(value);
      })
    })
  }

  deleteAppointment(id: any): Promise<void>{
    return this.realtimeDb.list<Appointment>('appointments').remove(id);
  }

  getRealtimeDbAppointments(): Observable<Appointment[]>{
    return this.realtimeDb.list<Appointment>('appointments').valueChanges();
  }

  async sendAppointment(eventCopy, uid){
      let newRef = this.realtimeDb.list<Appointment>('appointments').push({service: eventCopy.service, date: eventCopy.date, time: eventCopy.startTime, end: eventCopy.endTime, uid: uid, key: ''});
      var key = newRef.key;
      this.realtimeDb.list<Appointment>('appointments').update(key,{key: newRef.key});
  }

  getDatas(){
    return new Promise((resolve, reject) => {
      this.realtimeDb.list('data').valueChanges().subscribe(value => {
        resolve(value);
      })
    })
  }

  getDataFromServer(): Observable<SalonData[]>{
    return this.realtimeDb.list<SalonData>('data').valueChanges();
  }

  async sendDataToServer(updatedData, updatedValue){
    this.toArray();
    if (this.dataList.length == 0){
      let newRef = this.realtimeDb.list<SalonData>('data').push({intro: '', gelLakkTime: '', pedikurTime: '', mukoromTime: '', key: '', contactIntro: '', email: '', tel: ''});
      var key = newRef.key;
      if (updatedData == 'intro'){
        this.realtimeDb.list<SalonData>('data').update(key,{intro: updatedValue, key: key});
      } 
      if (updatedData == 'gelLakkTime'){
        this.realtimeDb.list<SalonData>('data').update(key,{gelLakkTime: updatedValue, key: key});
      }
      if (updatedData == 'pedikurTime'){
        this.realtimeDb.list<SalonData>('data').update(key,{pedikurTime: updatedValue, key: key});
      }
      if (updatedData == 'mukoromTime'){
        this.realtimeDb.list<SalonData>('data').update(key,{mukoromTime: updatedValue, key: key});
      }
      if (updatedData == 'contactIntro'){
        this.realtimeDb.list<SalonData>('data').update(key,{contactIntro: updatedValue, key: key});
      }
      if (updatedData == 'email'){
        this.realtimeDb.list<SalonData>('data').update(key,{email: updatedValue, key: key});
      }
      if (updatedData == 'tel'){
        this.realtimeDb.list<SalonData>('data').update(key,{tel: updatedValue, key: key});
      }
    } else {
      var getKey = this.dataList[0].key;
      if (updatedData == 'intro'){
        this.realtimeDb.list<SalonData>('data').update(getKey,{intro: updatedValue});
      } 
      if (updatedData == 'gelLakkTime'){
        this.realtimeDb.list<SalonData>('data').update(getKey,{gelLakkTime: updatedValue});
      }
      if (updatedData == 'pedikurTime'){
        this.realtimeDb.list<SalonData>('data').update(getKey,{pedikurTime: updatedValue});
      }
      if (updatedData == 'mukoromTime'){
        this.realtimeDb.list<SalonData>('data').update(getKey,{mukoromTime: updatedValue});
      }
      if (updatedData == 'contactIntro'){
        this.realtimeDb.list<SalonData>('data').update(getKey,{contactIntro: updatedValue});
      }
      if (updatedData == 'email'){
        this.realtimeDb.list<SalonData>('data').update(getKey,{email: updatedValue});
      }
      if (updatedData == 'tel'){
        this.realtimeDb.list<SalonData>('data').update(getKey,{tel: updatedValue});
      }
    }
  }
}