export const environment = {
  production: true,
  firebase: {
    projectId: 'roniszalon-edf98',
    appId: '1:319338469378:web:a7adfa46e2d5ba7047ef1b',
    storageBucket: 'roniszalon-edf98.appspot.com',
    locationId: 'europe-west',
    databaseURL: "https://roniszalon-edf98-default-rtdb.europe-west1.firebasedatabase.app",
    apiKey: 'AIzaSyAMxPSeEKLEAl5QUSvTBwo6l60hM2cDnWc',
    authDomain: 'roniszalon-edf98.firebaseapp.com',
    messagingSenderId: '319338469378',
    measurementId: 'G-3YCGBGKRG5',
    apiBase: 'http://prod-server:2080/app/',
    env: 'prod'
  }
};
