// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'roniszalon-edf98',
    appId: '1:319338469378:web:a7adfa46e2d5ba7047ef1b',
    storageBucket: 'roniszalon-edf98.appspot.com',
    locationId: 'europe-west',
    databaseURL: "https://roniszalon-edf98-default-rtdb.europe-west1.firebasedatabase.app",
    apiKey: 'AIzaSyAMxPSeEKLEAl5QUSvTBwo6l60hM2cDnWc',
    authDomain: 'roniszalon-edf98.firebaseapp.com',
    messagingSenderId: '319338469378',
    measurementId: 'G-3YCGBGKRG5',
    apiBase: 'http://dev-server:4200/app/',
    env: 'dev'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
